#include <stdio.h>
#include "RegEx/RegEx.h"
#include "Parsing/RecursiveDescentParser.h"

int main(int argc,char ** argv)
{
	//RegEx::RunTests();
	static const char *file[] =
	{"\n",
	 "\n",
	 "int main()\n",
	 "{\n",
	 "\tint x = 40;\n",
	 "\tint y = (x/20)*5;\n"
	 "\tint z = (x+y);\n",
	 "}\n",
	 nullptr
	};

	std::unique_ptr<Parser> pParser {new RecursiveDescentParser};

	pParser->Parse(file);

	return 0;
}