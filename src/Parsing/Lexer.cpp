#include "Parsing/Lexer.h"
#include "RegEx/RegEx.h"
#include <string>


Lexer::Lexer()
{
	Lexer::GenerateLexer();
}


/*
// TODO: Generate this from a file
enum class Token: char
{
	keFloatingPoint,
	keOpenBracket,
	keCloseBracket,
	keOpenBrace,
	keCloseBrace,
	keSquareBracketOpen,
	keSquareBracketClose,
	kePlus,
	keMinus,
	keStar,
	keLessThan,
	keGreaterThan,
	keDot,
	keIf,
	keElse
};*/


// TODO: Make this more data driven
void Lexer::GenerateLexer()
{
	const std::string whitespace {"((\n)+(\t)+( ))"};
	const std::string digit  {"(0-9)"};
	const std::string lower  {"(a-z)"};
	const std::string upper  {"(A-Z)"};
	const std::string bothUL {"(" + lower + "+" + upper + ")"};

	m_TokenMap.push_back({Token::keWhitespace, std::unique_ptr<RegEx>(new RegEx(whitespace.c_str()))});
		
	// Keywords
	m_TokenMap.push_back({Token::keKeywordChar, std::unique_ptr<RegEx>(new RegEx("char"))});
	m_TokenMap.push_back({Token::keKeywordInt, std::unique_ptr<RegEx>(new RegEx("int"))});
	m_TokenMap.push_back({Token::keKeywordVoid, std::unique_ptr<RegEx>(new RegEx("void"))});

	// Syntax (braces ect)
	m_TokenMap.push_back({Token::keOpenBrace,std::unique_ptr<RegEx>(new RegEx("{"))});
	m_TokenMap.push_back({Token::keCloseBrace, std::unique_ptr<RegEx>(new RegEx("}"))});
	m_TokenMap.push_back({Token::keOpenBracket, std::unique_ptr<RegEx>(new RegEx("\\("))});
	m_TokenMap.push_back({Token::keCloseBracket, std::unique_ptr<RegEx>(new RegEx("\\)"))});
	m_TokenMap.push_back({Token::keComma,      std::unique_ptr<RegEx>(new RegEx(","))});
	m_TokenMap.push_back({Token::keSemiColon,   std::unique_ptr<RegEx>(new RegEx(";"))});

	// Operators
	m_TokenMap.push_back({Token::keAssignment,  std::unique_ptr<RegEx>(new RegEx("="))});
	m_TokenMap.push_back({Token::keStar,        std::unique_ptr<RegEx>(new RegEx("\\*"))});
	m_TokenMap.push_back({Token::keForwardSlash,std::unique_ptr<RegEx>(new RegEx("/"))});
	m_TokenMap.push_back({Token::kePlus,       std::unique_ptr<RegEx>(new RegEx("\\+"))});
	m_TokenMap.push_back({Token::keMinus,      std::unique_ptr<RegEx>(new RegEx("-"))});

	std::string str = bothUL+std::string("((")+bothUL+std::string("+")+digit+std::string(")*)");
	std::string anyNumberOfDigits = digit+std::string("*");
	m_TokenMap.push_back({Token::keIdentifier, std::unique_ptr<RegEx>(new RegEx(str.c_str()))});
	m_TokenMap.push_back({Token::keInteger,    std::unique_ptr<RegEx>(new RegEx(anyNumberOfDigits.c_str()))});


}

// TODO: Maybe redo this using std::string/iterators?
void Lexer::Lex(const char ** data)
{
	int counter = 0;

	while (data[counter])
	{
		const char * line = data[counter];
		const char * cursor= line;

		int size = strlen(line);
		while ( strcmp(cursor,"") )
		{
			int matchSize   = 0;
			int maxMatchSize= 0;
			Lexer::Token matchingToken;

			for (auto &pair : m_TokenMap)
			{
				matchSize = pair.second->Match(cursor);

				// If this is at least a partial match
				if (matchSize != 0 && matchSize > maxMatchSize)
				{
					maxMatchSize = matchSize;
					matchingToken= pair.first;
				}

				// Complete match, exit early
				if (matchSize == size)
				{
					break;
				}
			}

			// No match found, mark as error
			if (maxMatchSize == 0)
			{
				matchingToken = Token::keError;
				maxMatchSize=1;

			}

			if (matchingToken != Lexer::Token::keWhitespace)
			{
				char buffer[maxMatchSize+1];
				memcpy(buffer,cursor,maxMatchSize);
				buffer[maxMatchSize] = '\0';
				std::string str {buffer};
				m_Lexemes.push_back( {matchingToken,str});
			}

			cursor = cursor+maxMatchSize;
		}
		counter++;
	}
}


static const char * GetTokenAsCharStar(Lexer::Token token)
{
	switch (token)
	{
		case Lexer::Token::keWhitespace:  return "Whitespace";
		case Lexer::Token::keIdentifier:  return "Identifier";
		case Lexer::Token::keAssignment:  return "Assignment";
		case Lexer::Token::keKeywordInt:  return "KeywordInt";
		case Lexer::Token::keKeywordChar: return "KeywordChar";
		case Lexer::Token::keOpenBrace:   return "OpenBrace";
		case Lexer::Token::keCloseBrace:  return "CloseBrace";
		case Lexer::Token::keOpenBracket: return "OpenBracket";
		case Lexer::Token::keCloseBracket:return "OpenBracket";
		case Lexer::Token::keComma:       return "Comma";
		case Lexer::Token::keSemiColon:   return "SemiColon";
		case Lexer::Token::keStar:        return "Star";
		case Lexer::Token::keInteger:     return "IntegerLiteral";
		case Lexer::Token::keError:       return "Error";
		case Lexer::Token::keForwardSlash:return "ForwardSlash";
		case Lexer::Token::keMinus:       return "Minus";
		case Lexer::Token::kePlus:        return "Add";
		default:                         return "Unknown";
	}
}

void Lexer::Dump() const
{
	printf("Lexer lexemes given by patten \"Lexeme<Token,MatchingString>\":\n");
	for (auto & pair : m_Lexemes)
	{

		printf("\tLexeme<%s,%s>\n",GetTokenAsCharStar(pair.first),pair.second.c_str());
	}
}
