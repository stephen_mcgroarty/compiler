#include "Parsing/RecursiveDescentParser.h"


RecursiveDescentParser::RecursiveDescentParser():m_pParseTree(nullptr)
{
	// The symbol '->' denotes a valid production and terminators
	// are given in '' marks. Non-terminators are given as
	// just as normal text. An add sign means concatination. Like so:
	// Example -> 'someToken' + someOtherGrammar

	// Type -> 'char'
	//      -> 'void'
	//      -> 'int'
	int index;
	std::shared_ptr<ParseTable> type {new ParseTable("Type") };

	// -> 'void'
	index = type->CreateProduction();
	type->AddTerminal(Lexer::Token::keKeywordVoid,index);

	// -> 'char'
	index = type->CreateProduction();
	type->AddTerminal(Lexer::Token::keKeywordChar,index);

	// -> 'int'
	index = type->CreateProduction();
	type->AddTerminal(Lexer::Token::keKeywordInt,index);



	// VariableDeclaration -> type + 'id'
	std::shared_ptr<ParseTable> variableDeclaration {new ParseTable("VariableDeclaration") };

	// -> type + 'id'
	index = variableDeclaration->CreateProduction();
	variableDeclaration->AddNonTerminal(type,index);
	variableDeclaration->AddTerminal(Lexer::Token::keIdentifier,index);



	// Epression -> Expression + '+' + Epression
	// 			 -> Expression + '*' + Epression
	// 			 -> Expression + '-' + Epression
 	// 			 -> Expression + '/' + Epression
	//           -> '(' + Epression + ')'
	//			 -> 'integerLiteral'
	//           -> 'ID'
	std::shared_ptr<ParseTable> expression {new ParseTable("Expression") };

 	// -> Expression + '+' + Epression
	index = expression->CreateProduction();
	expression->AddNonTerminal(expression,index);
	expression->AddTerminal(Lexer::Token::kePlus,index);
	expression->AddNonTerminal(expression,index);

 	// -> Expression + '*' + Epression
	index = expression->CreateProduction();
	expression->AddNonTerminal(expression,index);
	expression->AddTerminal(Lexer::Token::keStar,index);
	expression->AddNonTerminal(expression,index);

 	// -> Expression + '-' + Epression
	index = expression->CreateProduction();
	expression->AddNonTerminal(expression,index);
	expression->AddTerminal(Lexer::Token::keMinus,index);
	expression->AddNonTerminal(expression,index);

 	// -> Expression + '/' + Epression
	index = expression->CreateProduction();
	expression->AddNonTerminal(expression,index);
	expression->AddTerminal(Lexer::Token::keForwardSlash,index);
	expression->AddNonTerminal(expression,index);

	// -> '(' + Epression + ')'
	index = expression->CreateProduction();
 	expression->AddTerminal(Lexer::Token::keOpenBracket,index);
	expression->AddNonTerminal(expression,index);
 	expression->AddTerminal(Lexer::Token::keCloseBracket,index);

	// -> 'integerLiteral'
	index = expression->CreateProduction();
 	expression->AddTerminal(Lexer::Token::keInteger,index);

	// -> 'ID'
	index = expression->CreateProduction();
 	expression->AddTerminal(Lexer::Token::keIdentifier,index);


	// Assignment -> VariableDeclaration + '=' + Expression 
 	std::shared_ptr<ParseTable> assignment  {new ParseTable("Assignment") };

 	//  -> VariableDeclaration + '=' + Expression
 	index = assignment->CreateProduction();
 	assignment->AddNonTerminal(variableDeclaration,index);
	assignment->AddTerminal(Lexer::Token::keAssignment,index);
 	assignment->AddNonTerminal(expression,index);
	assignment->AddTerminal(Lexer::Token::keSemiColon,index);



	// FunctionArgsNonEmpty -> variableDeclaration + ',' + FunctionArgsNonEmpty
	//                      -> variableDeclaration
 	std::shared_ptr<ParseTable> functionArgsNonEmpty {new ParseTable("FunctionArgsNonEmpty") };

	// -> variableDeclaration + ',' + FunctionArgsNonEmpty
 	index = functionArgsNonEmpty->CreateProduction();
 	functionArgsNonEmpty->AddNonTerminal(variableDeclaration,index);
	functionArgsNonEmpty->AddTerminal(Lexer::Token::keComma,index);
 	functionArgsNonEmpty->AddNonTerminal(functionArgsNonEmpty,index);

	// -> variableDeclaration
 	index = functionArgsNonEmpty->CreateProduction();
 	functionArgsNonEmpty->AddNonTerminal(variableDeclaration,index);



 	// FunctionArgs -> FunctionArgsNonEmpty
 	//              -> epsilon
 	std::shared_ptr<ParseTable> functionArgs {new ParseTable("FunctionArgs") };

 	// -> FunctionArgsNonEmpty
 	index = functionArgs->CreateProduction();
 	functionArgs->AddNonTerminal(functionArgsNonEmpty,index);

	// -> epslion
 	index = functionArgs->CreateProduction();
 	functionArgs->AddEpsilon(index);


 	// TODO: Figure out what this should actually contain
 	// functionBody -> assignment
 	//				-> epsilon
	std::shared_ptr<ParseTable> functionBody {new ParseTable("FunctionBody") };

	// -> assignment
	index = functionBody->CreateProduction();
 	functionBody->AddNonTerminal(assignment,index);
 	functionBody->AddNonTerminal(functionBody,index);

	// -> epslion
 	index = functionBody->CreateProduction();
 	functionBody->AddEpsilon(index);



 	// Function -> type + 'id' + '(' + FunctionArgs + ')' + '{' + FunctionBody + '}'
	std::shared_ptr<ParseTable> function {new ParseTable("Function") };

	// -> type + 'id' + '(' + FunctionArgs + ')' + '{' + FunctionBody + '}'
	index = function->CreateProduction();
 	function->AddNonTerminal(type,index);
	function->AddTerminal(Lexer::Token::keIdentifier,index);
	function->AddTerminal(Lexer::Token::keOpenBracket,index);
 	function->AddNonTerminal(functionArgs,index);
 	function->AddTerminal(Lexer::Token::keCloseBracket,index);
	function->AddTerminal(Lexer::Token::keOpenBrace,index);
 	function->AddNonTerminal(functionBody,index);
	function->AddTerminal(Lexer::Token::keCloseBrace,index);



	// Module -> Function + Module
	//        -> Module
	//        -> espsilon
	std::shared_ptr<ParseTable> module {new ParseTable("Module") };

	//-> Function + Module
	index = module->CreateProduction();
 	module->AddNonTerminal(function,index);
 	module->AddNonTerminal(module,index);

	// -> Module
 	index = module->CreateProduction();
 	module->AddNonTerminal(function,index);

 	// -> espsilon
 	index = module->CreateProduction();
 	module->AddEpsilon(index);

 		

 	m_pParseTable = module;

}

void RecursiveDescentParser::Parse(const char ** data)
{
	m_Lexer.Lex(data);
	m_Lexer.Dump();

	int cursor = 0;
	m_pParseTree = m_pParseTable->Match(cursor,m_Lexer.GetLexemes(),"TreeRoot");
	
	if (m_pParseTree)
	{
		m_pParseTree->Dump();
	} else 
	{
		printf("Error: Parsing failed\n");
	}
}





