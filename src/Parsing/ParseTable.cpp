#include "Parsing/ParseTable.h"

ParseTable::ParseTable(const char * id): m_ID(id)
{}

int ParseTable::CreateProduction()
{
	int index = m_Productions.size();
	m_Productions.push_back({});
	return index;
}



void ParseTable::AddTerminal(Lexer::Token token,int production)
{
	m_Productions[production].push_back( Entry(token));
}

void ParseTable::AddNonTerminal(std::shared_ptr<ParseTable> table,int production)
{
	m_Productions[production].push_back( Entry(table));
}

void ParseTable::AddEpsilon(int production)
{
	m_Productions[production].push_back( Entry());
}

std::vector<Lexer::Token> ParseTable::GetNonTerminals() const
{
	std::vector<Lexer::Token> nonTerminals;

	for (auto & production : m_Productions)
	{
		if (production.size() > 1)
		{
			continue;
		}

		auto & entry = production[0];

		if (entry.type == 0)
		{
			nonTerminals.push_back(entry.token);

		}
	}

	return nonTerminals;
}


std::shared_ptr<ParseTree> ParseTable::Match(int &cursor, const std::vector<std::pair<Lexer::Token,std::string>> & tokens,const char * parentID,bool allowRecursion) const
{
	if (cursor >= tokens.size())
		return nullptr;

	for (auto & production : m_Productions)
	{
		// Save the position of the cursor so if a production fails,
		// we can reset the position.
		int save = cursor;

		// Attempt to generate a tree, only a correct production will be saved
		std::shared_ptr<ParseTree> tree { new ParseTree(m_ID) };

		for (int entryIndex =0; entryIndex < production.size(); ++entryIndex)
		{
			auto & entry = production[entryIndex];

			// Are we at the end of this production?
			bool end = entryIndex+1 == production.size();
		
			bool match = false;

			// Are we in a recursive loop?
			if ( entryIndex == 0       				// If this is the first entry in the production
				&& entry.type == 1     				// And this is a non-terminal
				&& m_ID == parentID    				// This is the same as parent
				&& entry.m_pTable->GetID()== m_ID   // and the next non-terminal is the same type
				&& !allowRecursion)                 // and the parent didn't move the cursor
			{
				break; // Break the recursion; force terminal resolution.
			}


			if (entry.type == 0) // Terminal
			{
				match = entry.token == tokens[cursor].first;
				if (match)
				{ 
					tree->AddChild(std::shared_ptr<ParseTree>(new ParseTree(tokens[cursor].second.c_str())));
					++cursor;
				}
			}
			 else if (entry.type == 1) // Non-Terminal
			{
				std::shared_ptr<ParseTree> temp = entry.m_pTable->Match(cursor,tokens,m_ID,!!entryIndex);

				// Only add to the tree if we have a match
				if (temp)
				{
					tree->AddChild(temp);
					match = true;
				}
			} 
			 else // Otherwise we are dealing with a epsilon, which always returns true.
			{
				match = true;
			}


			if (!match)
			{
				break; // If no match this production is invalid.
			}

			if (end) // If we are at the end of this production and have a match.
			{	
				return tree;
			}
		}

		// Reset the cursor
		cursor = save;
	}

	return nullptr;
}
