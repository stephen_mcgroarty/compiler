#include "Parsing/ParseTree.h"



void ParseTree::AddChild(std::shared_ptr<ParseTree> node)
{
	m_Children.push_back(node);
}


void ParseTree::Dump() const
{	
	std::string childFormat("  ");
	printf("%s\n",m_ID);

	for (auto child : m_Children)
	{
		child->Dump(childFormat);
	}
}


void ParseTree::Dump(std::string format) const
{
	printf("%s|%s\n",format.c_str(),m_ID);
	format.append("  ");
	for (auto child : m_Children)
	{
		child->Dump(format);
	}


}	
