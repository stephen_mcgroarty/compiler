#ifndef PARSE_TREE_H
#define PARSE_TREE_H
#include <memory>
#include <vector>
#include <string>

class ParseTree
{
public:
	ParseTree(const char * id): m_ID(id) {} 

	void AddChild(std::shared_ptr<ParseTree> node);

	void Dump() const;
public:

	const char * m_ID;
	void Dump(std::string format) const ;
	std::vector<std::shared_ptr<ParseTree>> m_Children;
};


#endif