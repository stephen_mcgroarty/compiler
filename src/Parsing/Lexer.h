#ifndef LEXER_H
#define LEXER_H
#include <vector>
#include <map>
#include <string>
#include "RegEx/RegEx.h"

class Lexer
{
public:
	Lexer();

	void Lex(const char ** data);

	void GenerateLexer();

	// TODO: Generate this from a file
	enum class Token: char
	{
		keWhitespace,
		keInteger,
		keIdentifier,
		keFloatingPoint,
		keOpenBracket,
		keCloseBracket,
		keOpenBrace,
		keCloseBrace,
		keSquareBracketOpen,
		keSquareBracketClose,
		keAssignment,
		kePlus,
		keMinus,
		keStar,
		keForwardSlash,
		keLessThan,
		keGreaterThan,
		keDot,
		keComma,
		keSemiColon,
		keKeywordInt,
		keKeywordChar,
		keKeywordVoid,
		keError
	};


	void Dump() const;
	const std::vector<std::pair<Token,std::string>> & GetLexemes() const { return m_Lexemes; }
private:

	// A map of tokens to the regular expressions which accept
	// those tokens
	std::vector<std::pair<Token,std::unique_ptr<RegEx>>> m_TokenMap;
	std::vector<std::pair<Token,std::string>> m_Lexemes;
};

#endif