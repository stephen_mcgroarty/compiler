#ifndef PARSE_TABLE_H
#define PARSE_TABLE_H
#include "Parsing/Lexer.h"
#include "Parsing/ParseTree.h"


/* 
 * A ParseTable is an implemntation of a Context Free Grammar.
 * Used primarily by the recursive descent parser.
 */
class ParseTable
{
public:

	ParseTable(const char * id);


	// Creates a new blank production and returns its id.
	int CreateProduction();

	void AddTerminal(Lexer::Token token,int production);
	void AddNonTerminal(std::shared_ptr<ParseTable> table,int production);
	void AddEpsilon(int production);


	// Get the list of non terminal productions
	std::vector<Lexer::Token> GetNonTerminals() const;

	std::shared_ptr<ParseTree> Match(int &cursor, const std::vector<std::pair<Lexer::Token,std::string>> & tokens, const char * parentID, bool allowRecursion = true) const;

	const char * GetID() const {return m_ID;}
private:

	// Unique ID assosiated with this Grammar/ParseTable
	const char * m_ID;

	// Entry can be a lexer token (terminal) or
	// another table (non terminal)
	struct Entry
	{
		Entry(Lexer::Token t):token(t),m_pTable(nullptr),type(0) {}
		Entry(std::shared_ptr<ParseTable> table):token(Lexer::Token::keError), m_pTable(table), type(1){}
		Entry(): token(Lexer::Token::keError),m_pTable(nullptr), type(2) {}
		
		Lexer::Token token;

		std::shared_ptr<ParseTable> m_pTable;	

		// 0: Token, 1: table, 2: epsilon (empty string)
		int type;
	};

	// The vector of entries is the actual table
	// and the vector of tables is the productions.
	std::vector< std::vector<Entry> > m_Productions;
};

#endif