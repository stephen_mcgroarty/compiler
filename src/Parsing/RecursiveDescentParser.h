#ifndef TOP_DOWN_PARSER_H
#define TOP_DOWN_PARSER_H
#include "Parsing/Parser.h"
#include <string>
#include "Parsing/ParseTable.h"


/*
 * Basic table driven recursive descent parser
 * TODO: Add in ability to detect parsing errors
*/
class RecursiveDescentParser: public Parser
{
public:

	RecursiveDescentParser();

	// Creates a parse tree
	void Parse(const char ** data);


private:	
	std::shared_ptr<ParseTable> m_pParseTable;
	std::shared_ptr<ParseTree>  m_pParseTree;
};


#endif