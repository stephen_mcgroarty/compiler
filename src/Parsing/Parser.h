#ifndef PARSER_H
#define PARSER_H
#include "Parsing/Lexer.h"

/* 
 * Parser is abstract to allow some experimenting with different types of parser.
*/
class Parser
{
public:

	Parser() {}

	// Creates a parse tree.
	virtual void Parse(const char ** data) = 0;

protected:
	Lexer m_Lexer;
};


#endif