#include "RegEx/RegEx.h"
#include "RegEx/NFA.h"

RegEx::RegEx(const char * pattern): m_pPattern(pattern) 
{

	NFA nfa(m_pPattern);

	m_pDFA = nfa.CreateDFA();
}



bool RegEx::DoesMatch(const char *text) const
{	
	if( m_pDFA )
	{
		return m_pDFA->Match(text);
	}
	else
	{
		printf("Error m_pDFA is NULL\n");
		return false;
	}
}



int RegEx::Match(const char *test) const
{
	if (m_pDFA)
	{
		return m_pDFA->GetMatchLength(test);
	}
	return 0;
}

bool RegEx::CheckTestCase(const std::tuple<TestResults,const char*,const char*> & testCase)
{
	RegEx regEx(std::get<1> (testCase));

	bool expectedResult = false;

	if( regEx.DoesMatch(std::get<2> (testCase)) )
	{
		if (!(std::get<0> (testCase) == RegEx::TestResults::kePASS))
			printf("Pattern: %s accepts string %s\n",std::get<1> (testCase),std::get<2> (testCase) );
		expectedResult = std::get<0> (testCase) == RegEx::TestResults::kePASS;
	} 
		else 
	{

		if (!(std::get<0> (testCase) == RegEx::TestResults::keFAIL))
			printf("Pattern: %s rejects string %s\n",std::get<1> (testCase),std::get<2> (testCase) );
		expectedResult =  std::get<0> (testCase) == RegEx::TestResults::keFAIL;
	}

	if (!expectedResult)
	{
		// regEx.Dump();
	}

	return expectedResult;
}

void RegEx::RunTests()
{
	std::vector<std::tuple<RegEx::TestResults,const char *,const char*>> testCases;
	
	// Test concatenation
	testCases.push_back( std::make_tuple(RegEx::TestResults::kePASS,"t","t"));
	testCases.push_back( std::make_tuple(RegEx::TestResults::kePASS,"test","test"));
	testCases.push_back( std::make_tuple(RegEx::TestResults::kePASS,"10101010","10101010"));
	testCases.push_back( std::make_tuple(RegEx::TestResults::keFAIL,"test","tests"));
	testCases.push_back( std::make_tuple(RegEx::TestResults::keFAIL,"test","tes"));
	testCases.push_back( std::make_tuple(RegEx::TestResults::keFAIL,"test","t"));

	// Test scoping
	testCases.push_back( std::make_tuple(RegEx::TestResults::kePASS,"(test)","test"));
	testCases.push_back( std::make_tuple(RegEx::TestResults::kePASS,"(t)","t"));
	testCases.push_back( std::make_tuple(RegEx::TestResults::kePASS,"(t)+(s)","t"));
	testCases.push_back( std::make_tuple(RegEx::TestResults::kePASS,"(t)+(s)","s"));

	testCases.push_back( std::make_tuple(RegEx::TestResults::keFAIL,"(test)","(test)"));
	testCases.push_back( std::make_tuple(RegEx::TestResults::kePASS,"(te(s)t)","test"));
	testCases.push_back( std::make_tuple(RegEx::TestResults::kePASS,"(((t(e)s(t)))","test"));
	

	// Test union
	testCases.push_back( std::make_tuple(RegEx::TestResults::kePASS,"((test)+(not))","test"));
	testCases.push_back( std::make_tuple(RegEx::TestResults::kePASS,"(test)+(not)","test"));
	testCases.push_back( std::make_tuple(RegEx::TestResults::kePASS,"((test)+(not))","not"));
	testCases.push_back( std::make_tuple(RegEx::TestResults::kePASS,"(test)+(not)","not"));
	testCases.push_back( std::make_tuple(RegEx::TestResults::keFAIL,"(test)+(not)","testnot"));
	testCases.push_back( std::make_tuple(RegEx::TestResults::keFAIL,"(test)+(not)","test+not"));
	testCases.push_back( std::make_tuple(RegEx::TestResults::keFAIL,"(test)+(not)","test+not"));
	testCases.push_back( std::make_tuple(RegEx::TestResults::keFAIL,"((test)+(not))","testot"));
	testCases.push_back( std::make_tuple(RegEx::TestResults::keFAIL,"((test)+(not))","tesnot"));
	testCases.push_back( std::make_tuple(RegEx::TestResults::kePASS,"(tes(t+n)ot)","tesnot"));
	testCases.push_back( std::make_tuple(RegEx::TestResults::kePASS,"(tes(t+n)ot)","testot"));

	testCases.push_back( std::make_tuple(RegEx::TestResults::kePASS,"(a+b+c+d+e+f+0+1+2+3+4+5+6+7+8+9)","a"));
	testCases.push_back( std::make_tuple(RegEx::TestResults::kePASS,"(a+b+c+d+e+f+0+1+2+3+4+5+6+7+8+9)","f"));
	testCases.push_back( std::make_tuple(RegEx::TestResults::kePASS,"(a+b+c+d+e+f+0+1+2+3+4+5+6+7+8+9)","0"));
	testCases.push_back( std::make_tuple(RegEx::TestResults::kePASS,"(a+b+c+d+e+f+0+1+2+3+4+5+6+7+8+9)","8"));
	testCases.push_back( std::make_tuple(RegEx::TestResults::kePASS,"(a+b+c+d+e+f+0+1+2+3+4+5+6+7+8+9)","7"));
	
	// Test actual use cases of union and multiple character DFA states
	testCases.push_back( std::make_tuple(RegEx::TestResults::kePASS,"(if)+(else)+(elif)+(case)","if"));
	testCases.push_back( std::make_tuple(RegEx::TestResults::kePASS,"(if)+(else)+(elif)+(case)","else"));
	testCases.push_back( std::make_tuple(RegEx::TestResults::kePASS,"(if)+(else)+(elif)+(case)","elif"));
	testCases.push_back( std::make_tuple(RegEx::TestResults::kePASS,"(if)+(else)+(elif)+(case)","case"));
	testCases.push_back( std::make_tuple(RegEx::TestResults::kePASS,"(else)+(elif)","else") );
	testCases.push_back( std::make_tuple(RegEx::TestResults::kePASS,"(else)+(elif)","elif") );
	
	// Test iterator
	testCases.push_back( std::make_tuple(RegEx::TestResults::kePASS,"1*","1"));
	testCases.push_back( std::make_tuple(RegEx::TestResults::kePASS,"1*",""));
	testCases.push_back( std::make_tuple(RegEx::TestResults::kePASS,"1*","1111111"));
	testCases.push_back( std::make_tuple(RegEx::TestResults::keFAIL,"1*","1111001"));

	testCases.push_back( std::make_tuple(RegEx::TestResults::keFAIL,"1*","1*"));
	testCases.push_back( std::make_tuple(RegEx::TestResults::keFAIL,"0*","0*"));

	// All together
	testCases.push_back( std::make_tuple(RegEx::TestResults::keFAIL,"0*1","000011"));
	testCases.push_back( std::make_tuple(RegEx::TestResults::kePASS,"0*1","000001"));
	testCases.push_back( std::make_tuple(RegEx::TestResults::kePASS,"(0+1)*","101000011001"));
	testCases.push_back( std::make_tuple(RegEx::TestResults::kePASS,"(0+1)*","001000011001"));
	testCases.push_back( std::make_tuple(RegEx::TestResults::keFAIL,"(0+1)*","0010000110002"));
	testCases.push_back( std::make_tuple(RegEx::TestResults::kePASS,"(0+1)*","0"));
	testCases.push_back( std::make_tuple(RegEx::TestResults::kePASS,"(0+1)*","1"));
	testCases.push_back( std::make_tuple(RegEx::TestResults::kePASS,"(0+1)*",""));

	testCases.push_back( std::make_tuple(RegEx::TestResults::kePASS,"(a+b+c+d+e+f+0+1+2+3+4+5+6+7+8+9)*","abba"));
	testCases.push_back( std::make_tuple(RegEx::TestResults::kePASS,"(a+b+c+d+e+f+0+1+2+3+4+5+6+7+8+9)*","12232abde9998"));
	testCases.push_back( std::make_tuple(RegEx::TestResults::kePASS,"(a+b+c+d+e+f+0+1+2+3+4+5+6+7+8+9)*",""));
	testCases.push_back( std::make_tuple(RegEx::TestResults::kePASS,"(a+b+c+d+e+f+0+1+2+3+4+5+6+7+8+9)*",""));

	testCases.push_back( std::make_tuple(RegEx::TestResults::kePASS,"(1+0)*1","11111"));
	testCases.push_back( std::make_tuple(RegEx::TestResults::kePASS,"(1+0)*1","00001"));
	testCases.push_back( std::make_tuple(RegEx::TestResults::keFAIL,"(1+0)*1","11110"));


	// Test escape character
	testCases.push_back( std::make_tuple(RegEx::TestResults::keFAIL,"1\\*","1"));
	testCases.push_back( std::make_tuple(RegEx::TestResults::keFAIL,"1\\*","11"));
	testCases.push_back( std::make_tuple(RegEx::TestResults::keFAIL,"1\\*",""));
	testCases.push_back( std::make_tuple(RegEx::TestResults::kePASS,"1\\*","1*"));

	testCases.push_back( std::make_tuple(RegEx::TestResults::kePASS,"(1\\+0)","1+0"));
	testCases.push_back( std::make_tuple(RegEx::TestResults::keFAIL,"(1\\+0)","1"));
	testCases.push_back( std::make_tuple(RegEx::TestResults::keFAIL,"(1\\+0)","0"));


	testCases.push_back( std::make_tuple(RegEx::TestResults::kePASS,"\\(1\\)","(1)"));
	testCases.push_back( std::make_tuple(RegEx::TestResults::keFAIL,"\\(1+0\\)","0"));
	testCases.push_back( std::make_tuple(RegEx::TestResults::keFAIL,"\\(1+0\\)","1"));
	testCases.push_back( std::make_tuple(RegEx::TestResults::kePASS,"\\(","("));
	testCases.push_back( std::make_tuple(RegEx::TestResults::kePASS,"\\)",")"));
	testCases.push_back( std::make_tuple(RegEx::TestResults::keFAIL,"\\(",""));
	testCases.push_back( std::make_tuple(RegEx::TestResults::keFAIL,"\\)",""));

	testCases.push_back( std::make_tuple( RegEx::TestResults::kePASS,"\\\\","\\"));
	testCases.push_back( std::make_tuple( RegEx::TestResults::keFAIL,"\\\\",""));
	
	
	// Test cases for range operator
	testCases.push_back( std::make_tuple(RegEx::TestResults::kePASS,"1-5","3"));
	testCases.push_back( std::make_tuple(RegEx::TestResults::kePASS,"1-5","1"));
	testCases.push_back( std::make_tuple(RegEx::TestResults::kePASS,"1-5","5"));
	testCases.push_back( std::make_tuple(RegEx::TestResults::keFAIL,"1-5","1-5"));
	testCases.push_back( std::make_tuple(RegEx::TestResults::kePASS,"1\\-5","1-5"));
	testCases.push_back( std::make_tuple(RegEx::TestResults::keFAIL,"1-5","11"));
	testCases.push_back( std::make_tuple(RegEx::TestResults::keFAIL,"1-5","12345"));
	testCases.push_back( std::make_tuple(RegEx::TestResults::keFAIL,"1-5","6"));
	testCases.push_back( std::make_tuple(RegEx::TestResults::keFAIL,"1-5","0"));
	testCases.push_back( std::make_tuple(RegEx::TestResults::keFAIL,"1-5","7"));
	testCases.push_back( std::make_tuple(RegEx::TestResults::keFAIL,"1-5","8"));
	//testCases.push_back( std::make_tuple(RegEx::TestResults::kePASS,"(a-z)*","aaaa"));
	
	testCases.push_back( std::make_tuple(RegEx::TestResults::kePASS,"((a-z)+(A-Z))*","Adsjfns"));
	testCases.push_back( std::make_tuple(RegEx::TestResults::kePASS,"((a-z)+(A-Z))*","dDSFS"));
	testCases.push_back( std::make_tuple(RegEx::TestResults::kePASS,"((a-z)+(A-Z))*","dsjfns"));
	testCases.push_back( std::make_tuple(RegEx::TestResults::kePASS,"((a-z)+(A-Z))*","ESFSDFSA"));

	testCases.push_back( std::make_tuple(RegEx::TestResults::kePASS,"((a-z)+(A-Z))*","zzassrderfsddsSECWJDNENDEJLND"));
	
	testCases.push_back( std::make_tuple(RegEx::TestResults::keFAIL,"((a-z)+(A-Z))*","zzassrderfsddsS212ECWJDNENDEJLND"));
	testCases.push_back( std::make_tuple(RegEx::TestResults::keFAIL,"((a-z)+(A-Z))*","zzassrderfsddsSECA-ZWJDNENDEJLND"));	
	

	int numberOfCases = testCases.size();
	int numPasses     = 0;
	for( auto & testCase : testCases)
	{
		if( CheckTestCase(testCase) )
			numPasses++;
	}

	printf("RegEx testing: %d/%d test cases pass. Pass rate:%.2f%%\n",numPasses,numberOfCases,(numPasses/static_cast<float>(numberOfCases))*100.0f );
}



void RegEx::Dump() const
{
	printf("Regex of pattern %s\n",m_pPattern);
	if (m_pDFA)
	{
		m_pDFA->Dump();
	}
	 else
	{
		printf("\tDFA is null\n");
	}

}