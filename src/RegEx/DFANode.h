#ifndef DFA_NODE_H
#define DFA_NODE_H
#include <map>
#include <vector>



class DFANodeTransition
{
public:
	DFANodeTransition(): m_Trigger('\0'),m_UpperRangeTrigger('\0'),m_Range(false) { }

	DFANodeTransition(char trigger): m_Trigger(trigger),m_UpperRangeTrigger('\0'),m_Range(false) { }
	
	DFANodeTransition(char lower,char upper): m_Trigger(lower),m_UpperRangeTrigger(upper),m_Range(true) { }

	bool operator<(const DFANodeTransition & other) const;

	bool IsRangeBased() const { return m_Range;}
	char GetTrigger()   const { return m_Trigger;}
	char GetUpperRange()const { return m_UpperRangeTrigger;}
private:
	
	// Works as both single character transition and as lower bounds
	// of range transition
	char m_Trigger;

	// Upper character of the range transition
	char m_UpperRangeTrigger;

	// Is this a character range?
	bool m_Range;
};


class DFANode
{
public:

	DFANode() : m_End(false) { }
	DFANode(bool end): m_End(end) { }

	void AddIndice(int index);
	
	bool ContainsIndice(int index) const;
	bool IndiceSetMatch(const std::vector<int> & indiceSet) const;
	bool IndiceSetMatch(const DFANode & other) const;

	void SetTable( const std::map<DFANodeTransition,int> & table ) { m_Table =table;}

	void SetNodeIndices(const std::vector<int> & indices) { m_NfaNodeIndices = indices; }

	void Dump() const;

	// Returns true if state accepts input char
	// index will be assigned to the index of the
	// next state
	bool CheckTable(char input,int & index) const;

	const std::vector<int> & GetIndiceSet() const { return m_NfaNodeIndices;}

	bool IsEnd() const { return m_End;}
	void SetEnd(bool end) { m_End = end;}
private:
	bool m_End; 
	// The indices of each nfa node that this amalgamated state replaces
	std::vector<int> m_NfaNodeIndices;

	// Map of char inputs to DFA node indices
	std::map<DFANodeTransition,int> m_Table;
};



#endif