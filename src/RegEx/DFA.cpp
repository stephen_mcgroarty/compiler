#include "RegEx/DFA.h"


DFA::DFA(){}

int DFA::GetMatchLength(const char * text) const
{
	int currentState = 0;
	int cursor       = 0;

	while (text[cursor] != '\0')
	{
		if ( !m_Nodes[currentState].CheckTable(text[cursor],currentState) )
		{
			break;
		}
		cursor++;
	}

	if( m_Nodes[currentState].IsEnd())
		return cursor;
	return 0;
}


bool DFA::Match(const char * text) const
{
	int currentState = 0;
	int cursor       = 0;

	while (text[cursor] != '\0')
	{
		if ( !m_Nodes[currentState].CheckTable(text[cursor],currentState) )
		{
			return false;
		}
		cursor++;
	}

	return m_Nodes[currentState].IsEnd();
}



void DFA::Dump() const
{
	for( int index =0; index < m_Nodes.size(); ++index)
	{
		printf("Node of index %d:\n",index );
		m_Nodes[index].Dump();
	}

}
