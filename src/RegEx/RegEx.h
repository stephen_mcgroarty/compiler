#ifndef REGEX_H
#define REGEX_H
#include "RegEx/DFA.h"
#include <tuple>

class RegEx
{
public:
	
	RegEx(const char * pattern);
	bool DoesMatch(const char *text) const;

	// How much of the string matches
	int Match(const char *text) const;

	void Dump() const;
	static void RunTests();
private:

	const char * m_pPattern;
	std::unique_ptr<DFA> m_pDFA;


	enum class TestResults: char
	{ 
		kePASS,
		keFAIL
	};


	static bool CheckTestCase(const std::tuple<TestResults,const char*,const char*> &);
};

#endif