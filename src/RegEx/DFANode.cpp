#include "RegEx/DFANode.h"


bool DFANode::CheckTable(char input,int & index) const
{
	auto itr = m_Table.find(input);
	if (itr == m_Table.end())
	{
		return false;
	}

	index = itr->second;
	return true;
}

void DFANode::AddIndice(int index)
{

	if (std::find(m_NfaNodeIndices.begin(),m_NfaNodeIndices.end(),index) != m_NfaNodeIndices.end() )
		m_NfaNodeIndices.push_back(index);
}

bool DFANode::IndiceSetMatch(const std::vector<int> & indiceSet) const
{
	bool match = true;
	for ( int index : indiceSet )
	{
		if (std::find ( m_NfaNodeIndices.begin(), m_NfaNodeIndices.end(), index) == m_NfaNodeIndices.end() )
		{
			match = false;
		}
	}
	return match;
}


bool DFANode::IndiceSetMatch(const DFANode & other) const
{
	return IndiceSetMatch(other.GetIndiceSet());

}

bool DFANode::ContainsIndice(int index) const
{
	return std::find(m_NfaNodeIndices.begin(), m_NfaNodeIndices.end(), index) != m_NfaNodeIndices.end(); 
}


void DFANode::Dump() const
{
	printf("\tNode indices:{");
	for( auto indice:m_NfaNodeIndices)
	{
		printf("%d ",indice);
	}
	printf("}\n");

	printf("\t\tConnection Table:\n");

	for (auto& pair: m_Table)
	{
		printf("\t\t\tTrigger\tConnection\n");

		if (!pair.first.IsRangeBased())
		{
			printf("\t\t\t%c\t%d\n",pair.first.GetTrigger(),pair.second);
		} else 
		{
			printf("\t\t\t%c-%c\t%d\n",pair.first.GetTrigger(),pair.first.GetUpperRange(),pair.second);

		}
	}

	printf("\t\tIs end state=%d\n",m_End);
}



bool DFANodeTransition::operator<(const DFANodeTransition & rhs) const
{

	if (!rhs.m_Range && !m_Range)
	{
		return m_Trigger < rhs.m_Trigger;
	}

    if (!rhs.m_Range)
    {
		return rhs.m_Trigger < m_Trigger || rhs.m_Trigger > m_UpperRangeTrigger;
	}

	if (!m_Range)
	{	

		return m_Trigger < rhs.m_Trigger || m_Trigger > rhs.m_UpperRangeTrigger;
	}		


	return m_Trigger < rhs.m_Trigger || m_UpperRangeTrigger > rhs.m_UpperRangeTrigger;


}