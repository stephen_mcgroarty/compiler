#ifndef NFA_NODE_H
#define NFA_NODE_H
#include <memory>
#include <vector>
#include <map>

class NFANode
{
public:


	enum class TransitionType: char
	{
		keEpsilon,
		keTrigger,
		keRange
	};

	// TODO: Clean up the way this works
	struct NFANodeTransition
	{
		NFANodeTransition(std::shared_ptr<NFANode> n): type(TransitionType::keEpsilon),node(n) {}
		
		NFANodeTransition(char t,std::shared_ptr<NFANode> n): type(TransitionType::keTrigger),node(n)
		{
			trigger[0] = t;
			trigger[1] = '\0';
		} 
		
		NFANodeTransition(char t[2],std::shared_ptr<NFANode> n): type(TransitionType::keRange),node(n)
		{
			trigger[0] = t[0];
			trigger[1] = t[1];
		} 

		// Uses both if it is a range transition, otherwise 
		// triggered by the first character
		char trigger[2];

		TransitionType type;

		// The node that this transition connects to
		std::shared_ptr<NFANode> node;
	};


	NFANode();

	// Sometimes we don't want a unique index like 
	// for the epsilon closure
	NFANode(int index);

	// Build machine which accepts characters in any given range
	static std::shared_ptr<NFANode> BuildMachine(char rangeLower,char rangeUpper);
	static std::shared_ptr<NFANode> BuildMachine(char trigger);
	static std::shared_ptr<NFANode> BuildUnion(std::shared_ptr<NFANode> machineA,std::shared_ptr<NFANode> machineB);
	static std::shared_ptr<NFANode> BuildConcatenation(std::shared_ptr<NFANode> machineA,std::shared_ptr<NFANode> machineB);
	static std::shared_ptr<NFANode> BuildIteration(std::shared_ptr<NFANode> machine);
	// Moves through this machine and return the first end node
	// TODO: Support multiple end nodes? 
	std::shared_ptr<NFANode> GetEndNode();


	// Move through this node structure and return a list of its contents
	void GetNodeList(std::vector<std::shared_ptr<NFANode>> & list);

	// Return the epsilon closure of *all* nodes within this structure
	void GetEpsilonClosureList(std::vector<std::shared_ptr<NFANode>> & list);

	// Return the epsilon closure of this node
	std::shared_ptr<NFANode> GetEpsilonClosure() const;

	// Move through the attached nodes and return a mapping of inputs to nodes
	void GetNodeMap(std::map<int,std::shared_ptr<NFANode>> & map);

	// Get the node of index which is attached to this node
	std::shared_ptr<NFANode> GetNode(int index);

	bool GetEnd() const   { return m_End;}
	void SetEnd(bool end) { m_End = end; }
	std::vector<NFANodeTransition> & GetTransitions() { return m_Transitions; }

	void SetTransitions(const std::vector<NFANodeTransition> & trans) { m_Transitions = trans;}

	int GetIndex() const { return m_Index;}
private:


	std::vector<NFANodeTransition> m_Transitions;

	// Number of nodes which have been created
	static int S_NUMBER_OF_CREATED_STATES;


	// Unique index assigned to this node
	int m_Index;

	// Is this an accepting state
	bool m_End;
};

#endif