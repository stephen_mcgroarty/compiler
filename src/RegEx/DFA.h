#ifndef DFA_H
#define DFA_H
#include "RegEx/DFANode.h"

class DFA
{
public:

	DFA();

	bool Match(const char * text) const;

	// Get the length of the pattern which is accepted by this
	// DFA
	int GetMatchLength(const char * text) const;
	void Dump() const;

	void SetNodes ( std::vector<DFANode> & nodes) { m_Nodes = nodes; }
private:
	std::vector<DFANode> m_Nodes;
};
#endif