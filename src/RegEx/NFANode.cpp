#include "RegEx/NFANode.h"

int NFANode::S_NUMBER_OF_CREATED_STATES = 0;


NFANode::NFANode():
	m_Index(S_NUMBER_OF_CREATED_STATES++),
	m_End(false)
{}

NFANode::NFANode(int index):
	m_Index(index),
	m_End(false)
{}


// Build a machine which accepts only the character "trigger"
std::shared_ptr<NFANode> NFANode::BuildMachine(char trigger)
{
	auto pNode    = std::shared_ptr<NFANode>( new NFANode() );
	auto nextNode = std::shared_ptr<NFANode>( new NFANode() );

	nextNode->SetEnd(true);
	pNode->m_Transitions.push_back( NFANodeTransition(trigger,nextNode) );
	return pNode;
}



// Build a machine which accepts characters between the range
std::shared_ptr<NFANode> NFANode::BuildMachine(char rangeLower,char rangeUpper)
{
	auto pNode    = std::shared_ptr<NFANode>( new NFANode() );
	auto nextNode = std::shared_ptr<NFANode>( new NFANode() );

	nextNode->SetEnd(true);

	char buffer[2] = {rangeLower,rangeUpper};
	pNode->m_Transitions.push_back( NFANodeTransition(buffer,nextNode) );
	return pNode;
}

// Machine (A+B)
std::shared_ptr<NFANode> NFANode::BuildUnion(std::shared_ptr<NFANode> machineA,std::shared_ptr<NFANode> machineB)
{

	std::shared_ptr<NFANode> pMachineAEnd = machineA->GetEndNode();
	pMachineAEnd->m_End = false;

	std::shared_ptr<NFANode> pMachineBEnd = machineB->GetEndNode();
	pMachineBEnd->m_End = false;


	auto pNewNode = machineA;
	machineA = std::shared_ptr<NFANode>( new NFANode() );
	

	machineA->m_Transitions.push_back( NFANodeTransition(pNewNode) );
	machineA->m_Transitions.push_back( NFANodeTransition(machineB) );


	auto pNewEndNode =std::shared_ptr<NFANode>( new NFANode() );
	pNewEndNode->m_End = true;


	pMachineAEnd->m_Transitions.push_back( NFANodeTransition(pNewEndNode));
	pMachineBEnd->m_Transitions.push_back( NFANodeTransition(pNewEndNode));
	return machineA;
}

// Machine AB
std::shared_ptr<NFANode> NFANode::BuildConcatenation(std::shared_ptr<NFANode> machineA,std::shared_ptr<NFANode> machineB)
{
	std::shared_ptr<NFANode> pMachineAEnd = machineA->GetEndNode();
	pMachineAEnd->m_End = false;
	pMachineAEnd->m_Transitions.push_back( NFANodeTransition (machineB) );
	return machineA;
}

//Machine A*
std::shared_ptr<NFANode> NFANode::BuildIteration(std::shared_ptr<NFANode> machine)
{
	std::shared_ptr<NFANode> pMachineEnd = machine->GetEndNode();
	pMachineEnd->m_End = false;

	std::shared_ptr<NFANode> pNewStartState {new NFANode()};

	std::shared_ptr<NFANode> pNewEndState { new NFANode()};
	pNewEndState->m_End = true;
	
	pMachineEnd->m_Transitions.push_back( NFANodeTransition(pNewEndState));
	pMachineEnd->m_Transitions.push_back( NFANodeTransition(pNewStartState));
	

	pNewStartState->m_Transitions.push_back( NFANodeTransition(pNewEndState));
	pNewStartState->m_Transitions.push_back( NFANodeTransition(machine));

	return pNewStartState;
}



std::shared_ptr<NFANode> NFANode::GetNode(int index)
{
	if ( m_Index == index ) 
		return std::shared_ptr<NFANode>(new NFANode(*this));

	for (auto &transition : m_Transitions)
	{
		auto returnedNode = transition.node->GetNode(index);
		if (returnedNode)
			return returnedNode;
	}
	return nullptr;
}

std::shared_ptr<NFANode> NFANode::GetEndNode ()
{
	if ( m_End )
		return std::shared_ptr<NFANode>(new NFANode(*this));

	for ( auto transition : m_Transitions )
	{
		if( transition.node->m_End )
			return transition.node;

		std::shared_ptr<NFANode> recursiveSearch = transition.node->GetEndNode( );
		if (recursiveSearch != nullptr)
			return recursiveSearch;
	}
	return nullptr;
}

  
std::shared_ptr<NFANode> NFANode::GetEpsilonClosure() const
{
	auto epsilonClosure = std::shared_ptr<NFANode>(new NFANode(m_Index));
	
	// The list of states given by epsilon closure need to
	// have all the same data members minus the transitions
	// which obviously will be comprised of only epsilon
	// transitions
	epsilonClosure->m_End = m_End;

	for ( auto transition : m_Transitions )
	{
		// If triggered by an epsilon
		if(transition.type == NFANode::TransitionType::keEpsilon ) 
		{
			epsilonClosure->m_Transitions.push_back( NFANodeTransition(transition.node->GetEpsilonClosure()) );
		}
	}

	return epsilonClosure;
}


void NFANode::GetNodeList( std::vector<std::shared_ptr<NFANode>> & list ) 
{
	list.push_back(std::shared_ptr<NFANode>(new NFANode(*this)));

	for ( auto transition : m_Transitions)
	{
		transition.node->GetNodeList(list);
	}
}

void NFANode::GetEpsilonClosureList(std::vector<std::shared_ptr<NFANode>> & list)
{
	std::vector<std::shared_ptr<NFANode>> nodeList;

	GetNodeList(nodeList);

	for (auto &node : nodeList)
	{
		std::shared_ptr<NFANode> epsilonClosure = node->GetEpsilonClosure();
		
		if (epsilonClosure)
		{
			list.push_back(epsilonClosure);
		}
	}

}


void NFANode::GetNodeMap(std::map<int,std::shared_ptr<NFANode>> & map)
{
	if (map.find(m_Index) == map.end())
	{
		map[m_Index] = std::shared_ptr<NFANode>(new NFANode(*this));
	}

	for (auto transition : m_Transitions)
	{
		if (map.find(transition.node->GetIndex()) == map.end() )
		{
			transition.node->GetNodeMap(map);
		}
	}
}