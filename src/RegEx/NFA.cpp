#include "RegEx/NFA.h"
#include <algorithm>
#include "RegEx/DFA.h"
NFA::NFA(const char * pattern): m_pStartState(nullptr), m_pPattern(pattern),m_Position(0)
{
	DeriveAlphabet ();
	Build ();
}

void NFA::DeriveAlphabet() 
{
	int index = 0;
	while (m_pPattern[index] != '\0')
	{
		if (std::find(m_Alphabet.begin(),m_Alphabet.end(),m_pPattern[index]) == m_Alphabet.end() )
		{
			m_Alphabet.push_back(m_pPattern[index]);
		}
		index++;
	}
}


/*
 *	Moves over the pattern recursively building up the NFA
 *  from sub machines within the pattern
 */ 
void NFA::Build()
{
	m_Position = 0; // Current position in the pattern 
	m_pStartState = BuildSubmachine();

}


std::shared_ptr<NFANode> NFA::BuildSubmachine()
{
	std::shared_ptr<NFANode> pMachineRoot = nullptr;

	while (m_pPattern[m_Position] != '\0')
	{
		// Start a new submachine
		if (m_pPattern[m_Position] == '(')
		{
			// Stop the next submachine from reading this '('
			m_Position++;

			// If we are in the first node of *this* submachine
			if (!pMachineRoot)
			{
				pMachineRoot = BuildSubmachine();
			}
			else
			{
				// Concatenate this machine with the next submachine
				pMachineRoot = NFANode::BuildConcatenation(pMachineRoot,BuildSubmachine());
			}
			continue;
		}

		// This submachine is done, return it
		if ( m_pPattern[m_Position] == ')')
		{
			// Stop the next machine from reading this ')' again
			m_Position++;
			return pMachineRoot;
		}


		// Check if the escape character "\" has been input
		bool isEscapeChar = m_pPattern[m_Position] == '\\';
		if (isEscapeChar)
		{
			m_pPattern++;
		}


		// Check if this is accepting character range
		bool isRange = false;
		if (m_pPattern[m_Position+1] == '-')
		{
			isRange = true;
		}


		std::shared_ptr<NFANode> nextNode;
		if (!isRange)
		{

			if (m_pPattern[m_Position] == '*' && !isEscapeChar)
			{
				// Will be created in next call to create submachine
				nextNode = nullptr;
			} 
				else if(m_pPattern[m_Position] == '+' && !isEscapeChar && m_pPattern[m_Position+1] != '(')
			{
				nextNode = NFANode::BuildMachine(m_pPattern[m_Position+1]);
			} 
				else
			{
				nextNode = NFANode::BuildMachine(m_pPattern[m_Position]);
			}
		}
		else
		{
			nextNode = NFANode::BuildMachine(m_pPattern[m_Position],m_pPattern[m_Position+2]);
			m_Position+=2;
		}
		


		// If this machine hasn't been started yet, start it
		if (!pMachineRoot)
		{
			pMachineRoot = nextNode;
		}
			else if (m_pPattern[m_Position] == '+' && !isEscapeChar)
		{
			m_Position++;
			if (m_pPattern[m_Position] != '(')
			{
				pMachineRoot = NFANode::BuildUnion(pMachineRoot,nextNode);
				
				// We've now created a node for the this element, move on 
				m_Position++;
			}
			 else 
			{

				// Skip the '(' in the next call to BuildSubmachine
				m_Position++;
				pMachineRoot = NFANode::BuildUnion(pMachineRoot,BuildSubmachine());
			}
			continue; 
		}
			else if (m_pPattern[m_Position] == '*' && !isEscapeChar)
		{
			pMachineRoot = NFANode::BuildIteration(pMachineRoot);			
		}
			else
		{
			pMachineRoot = NFANode::BuildConcatenation(pMachineRoot,nextNode);
		}

		m_Position++;
	}
	return pMachineRoot;
}

std::unique_ptr<DFA> NFA::CreateDFA()
{
	std::unique_ptr<DFA> dfa = std::unique_ptr<DFA>(new DFA);
	

	m_pStartState->GetNodeMap(m_NodeMap);

	std::vector<DFANode> dfaNodes;
	CreateDFANode(m_pStartState,dfaNodes);


	dfa->SetNodes(dfaNodes);
	return dfa;
}


int NFA::CreateDFANode(std::shared_ptr<const NFANode> nfaNode,std::vector<DFANode> &dfaNodes)
{
	std::shared_ptr<NFANode> pEpsilonClosure = nfaNode->GetEpsilonClosure();

	std::vector<std::shared_ptr<NFANode>>  list;

	pEpsilonClosure->GetNodeList(list);

	std::vector<int> indices;

	for (auto &node : list)
	{
		indices.push_back(node->GetIndex());
	}
	return CreateDFANode(indices,dfaNodes);
}



// Creates a single DFA node from the Indices
int NFA::CreateDFANode(const std::vector<int> & indices,std::vector<DFANode> &dfaNodes)
{
	DFANode newDfaNode = DFANode();
	newDfaNode.SetNodeIndices(indices);

	// Check if this node already exists
	for (int index =0; index < dfaNodes.size(); ++index)
	{
		DFANode dfaNode = dfaNodes[index];
		if ( dfaNode.IndiceSetMatch(newDfaNode) )
		{
			// We already have an existing DFA for this node, just return it
			return index;
		}
	}

	int nodeIndex = dfaNodes.size();
	dfaNodes.push_back(newDfaNode);

	std::map<DFANodeTransition,int> transitionMap;

	// The indices that this dfa state can transition to on
	// any given input
	std::map<DFANodeTransition,std::vector<int>> nextStateIndices;


	// Are any of these NFA nodes accepting states?
	bool end = false;
	for (auto actualNodeIndice : indices)
	{
		auto actualNode = m_NodeMap[actualNodeIndice];

		// Is this node an accepting node
		if (actualNode->GetEnd())
		{
			end = true;
		}


		for (auto & transition : actualNode->GetTransitions())
		{
			if (transition.type != NFANode::TransitionType::keEpsilon)
			{
				// Create the transition type based on whether or not we are
				// dealing with a range or a single character transition 
				DFANodeTransition dfaTransition;
				if (transition.type == NFANode::TransitionType::keTrigger)
				{
					dfaTransition = DFANodeTransition(transition.trigger[0]);
				} 
				else 
				{
					dfaTransition = DFANodeTransition(transition.trigger[0],transition.trigger[1]);
				}


				if (nextStateIndices.find(dfaTransition) == nextStateIndices.end())
				{
					nextStateIndices.insert({dfaTransition,{transition.node->GetIndex()}});
				}
					else
				{
					nextStateIndices[dfaTransition].push_back (transition.node->GetIndex());
				}
			}
		}
	}


	dfaNodes[nodeIndex].SetEnd(end);

	for (auto &pair : nextStateIndices)
	{
		for ( int index =0; index < pair.second.size(); ++index)
		{
			auto actualNodeEpsilonClosure = m_NodeMap[pair.second[index]]->GetEpsilonClosure();
			std::vector<std::shared_ptr<NFANode>>  list;
			actualNodeEpsilonClosure->GetNodeList(list);

			for (auto & node : list)
			{
				if (std::find(pair.second.begin(),pair.second.end(),node->GetIndex()) == pair.second.end())
					pair.second.push_back (node->GetIndex());
			}
		}

		transitionMap.insert( {pair.first, CreateDFANode(pair.second,dfaNodes)});
	}

	dfaNodes[nodeIndex].SetTable(transitionMap);
	return nodeIndex;
}


