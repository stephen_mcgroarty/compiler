#ifndef NFA_H
#define NFA_H
#include "RegEx/NFANode.h"
class DFA;
class DFANode;


class NFA
{
public:
	NFA(const char * pattern);
	std::unique_ptr<DFA> CreateDFA();


private:

	//Builds the NFA to match the pattern
	void Build();

	// Creates a single DFA node from the nfaNode, adds it to the nodes list and returns it's index in the list
	int CreateDFANode(std::shared_ptr<const NFANode> nfaNode,std::vector<DFANode>& nodes);

	// Creates a single DFA node from the nfaIndices
	int CreateDFANode(const std::vector<int> & nfaIndices,std::vector<DFANode>& nodes);


	// Builds a submachine and returns upon encountering a close bracket')'
	std::shared_ptr<NFANode> BuildSubmachine();
	std::shared_ptr<NFANode> m_pStartState;

	void DeriveAlphabet();

	// The pattern the NFA is to be built from
	const char * m_pPattern;

	// Position in the pattern
	int m_Position;


	std::map<int,std::shared_ptr<NFANode>> m_NodeMap;
	std::vector<char> m_Alphabet;
};

#endif